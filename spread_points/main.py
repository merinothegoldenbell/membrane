from lipid import Lipid
from spread_points_3D import points_coord
import numpy as np

z0 = -5.0
z1 = -5.0
box_x, box_y, box_z = 15.0, 15.0, 15.0

N = 64 #lipid count in one layer; was 64
rho = 1 #density of particles; was 3
n_total = int(box_x*box_y*box_z*rho)
n_water = n_total - 2 * N * 11

# попробовать общий пул координат
# x_total, y_total, z_total = points_coord(n_water + 2 * N, box_x, box_y, box_z)
# x_head, y_head = x_total[:N], y_total[:N]
# x_head1, y_head1 = x_total[N:2*N], y_total[N:2*N]
# xw, yw, zw = x_total[2*N:], y_total[2*N:], z_total[2*N:]

x_head, y_head, z_head = points_coord(N, box_x, box_y)
x_head1, y_head1, z_head1 = points_coord(N, box_x, box_y) #second layer
L = Lipid()
x, y, z = [], [], []
types = []

for i in range(len(x_head)):
    L.create(x_head[i], y_head[i], z0, -2.0)
    x += list(L.coords[:,0]) #use blist???
    y += list(L.coords[:,1])
    z += list(L.coords[:,2])
    types += L.types
    if i == 0:
        bonds = L.bonds + 1
    else:
        bonds = np.vstack([bonds, L.bonds + 1 + i*11*2])
    L.create(x_head1[i], y_head1[i], z1, 2.0)
    x += list(L.coords[:,0])
    y += list(L.coords[:,1])
    z += list(L.coords[:,2])
    types += L.types
    bonds = np.vstack([bonds, L.bonds + 12 + i*11*2])

#xw, yw, zw = points_coord(15, box_x, box_y, box_z, max_iteration=100)

with open('lipids4.ent', 'w') as f:
    n = 0
    for xt, yt, zt, t in zip(x, y, z, types):
        n += 1
        f.writelines(f'HETATM{n:5d}  {t}{n:12d}{xt:12.3f}{yt:8.3f}{zt:8.3f}\n')
    # for xw, yw, zw in zip(xw, yw, zw):
    #      n += 1
    #      f.writelines(f'HETATM{n:5d}  H{n:12d}{xw:12.3f}{yw:8.3f}{zw:8.3f}\n')
    for bond in bonds:
        f.writelines(f'CONECT{bond[0]:5d}{bond[1]:5d}\n')

with open('BONDS_exp.ent', 'w') as f:
    f.writelines(f' num_bonds{len(bonds):13d}  num_atoms{n_total:13d}  BOX{box_x:14.7f}{box_y:17.7f}{box_z:17.7f}\n')
    for bond in bonds:
        f.writelines(f'{bond[0]:12d}{bond[1]:12d}\n')

with open('COORD_exp.ent', 'w') as f:
    f.writelines(f'num_atoms{n_total:7d} box size{box_x:15.10f}{box_y:17.10f}{box_z:17.10f}\n')
    n = 0
    for xt, yt, zt, t in zip(x, y, z, types):
        n += 1
        f.writelines(f'{n:12d}  {xt:13.10f}  {yt:13.10f}  {zt:13.10f} {t}\n') #поменять формат записи
    # for xw, yw, zw in zip(xw, yw, zw):
    #      n += 1
    #      f.writelines(f'{n:12d}  {xw:13.10f}  {yw:13.10f}  {zw:13.10f} H\n') #поменять формат записи

with open('ANGLS_exp.ent', 'w') as f:
    f.writelines(f' num_angl{(6 * N * 2):13d}\n')
    for i in range(1, N * 2 * 11 - 1, 11):
        f.writelines(
            f'{(i + 1):12d}{(i + 3):12d}{(i + 4):12d}\n'
            f'{(i + 3):12d}{(i + 4):12d}{(i + 5):12d}\n'
            f'{(i + 4):12d}{(i + 5):12d}{(i + 6):12d}\n'
            f'{(i + 2):12d}{(i + 7):12d}{(i + 8):12d}\n'
            f'{(i + 7):12d}{(i + 8):12d}{(i + 9):12d}\n'
            f'{(i + 8):12d}{(i + 9):12d}{(i + 10):12d}\n'
            )

with open('FIELD_exp.ent', 'w') as f:
    f.writelines(
        f'   num_types {len(np.unique(types)) + 1}\n'
        '   25.0   30.0   75.0\n'
        '   30.0   30.0   35.0\n'
        '   75.0   35.0   10.0'
        )

dt = 0.01
l_bond = 0.5
k_bond = 128.0
k_ang = 15.0
num_step = 5000
num_snapshot = 1000
num_track = 500


with open('CONTR_exp.ent', 'w') as f:
    f.writelines(
        f'{dt}  | dt           ! временной шаг интегрирования\n'
        f'{l_bond}    | l_bond       ! длина связи\n'
        f'{k_bond}   | k_bond       ! жёсткость связи\n'
        f'{k_ang}   | k_ang\n'
        f'{num_step}  | num_step     ! число шагов\n'
        f'{num_snapshot}  | num_snapshot ! через какое число шагов делать снимок\n'
        f'{num_track}  | num_track    ! через какое число шагов печатать траекторию\n'
        )